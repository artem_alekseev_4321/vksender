﻿using System;

using VkNet;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VkNet.AudioBypassService.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace VKSender
{
    class Program
    {
        static void Main(string[] args)
        {
            // обход ограничений вк на отправку сообщений
            var services = new ServiceCollection();
            services.AddAudioBypass(); 
            
            // создание экземпляра api, через который производим запросы
            var api = new VkApi(services);
            // Авторизация
            api.Authorize(new ApiAuthParams
            {
                Login = "+7...",
                Password = "password"
            });
            string message = "";
            while (true)
            {
                // ввод сообщения
                Console.WriteLine(message);
                Console.WriteLine(
                    "Введите сообщение, которое нужно отправить всем подписчикам или quit для выхода из программы");
                message = Console.ReadLine();
                
                // выход
                if (message.Equals("quit"))
                {
                    break;
                }
                
                // ввод количества жертв
                Console.WriteLine(
                    "Скольким людям необходимо отослать сообщение?");
                if(!int.TryParse(Console.ReadLine(), out int num))
                {
                    Console.WriteLine("Invalid value entered");
                }
                else
                {
                    Console.WriteLine("You entered {0}", num);
                }
                
                // получение списка всех пользователей группы (без подробной информации, только id)
                var users = api.Groups.GetMembers(new GroupsGetMembersParams
                {
                    GroupId = "51812607", // id  группы, узнавать тут https://regvk.com/id/
                });

                for (int i = 0; i < num; i++) // урезание числа сообщений
                {
                    User user = users[i];
                    // получение подробной информации (имя, фамилия и прочее, нужно для логирования)
                    var user_info = api.Users.Get(new long[]
                    {
                        user.Id, // id  группы
                    });
                    Random rnd = new Random();  // требуется с каждым запросом отправлять рандомное число
                    try
                    {
                        // отправка сообщения
                        var x = api.Messages.Send(new VkNet.Model.RequestParams.MessagesSendParams
                        {
                            RandomId = rnd.Next(100000, 10000000),
                            Message = message,
                            PeerId = user.Id
                        });
                        Console.WriteLine("OK for user {0} {1} (id = {2})", user_info[0].FirstName, user_info[0].LastName, user.Id);
                    }
                    catch
                    {
                        Console.WriteLine("FAIL for user {0} {1} (id = {2})", user_info[0].FirstName, user_info[0].LastName, user.Id);
                    }

                }
            }
        }
    }
}